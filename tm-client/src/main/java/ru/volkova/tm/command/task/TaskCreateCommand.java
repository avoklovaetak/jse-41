package ru.volkova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

public class TaskCreateCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "create new task";
    }

    @Override
    public void execute() {
        if (bootstrap == null) throw new ObjectNotFoundException();
        if (endpointLocator == null) throw new ObjectNotFoundException();
        @Nullable final Session session = bootstrap.getSession();
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        @Nullable final String description = TerminalUtil.nextLine();
        endpointLocator.getTaskEndpoint().addTaskByUser(session, name, description);
    }

    @NotNull
    @Override
    public String name() {
        return "task-create";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
