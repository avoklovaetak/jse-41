package ru.volkova.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.entity.IWBS;

import javax.persistence.Column;
import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "task")
@NoArgsConstructor
public class Task extends AbstractOwnerEntity implements IWBS {

    @Column(name = "project_id")
    @Nullable
    private String projectId;

    @NotNull
    public String toString() {
        return id + ": " + getName() + ": " + projectId
                + "; " + "created: " + getCreated() +
                "started: " + getDateStart();
    }

}
