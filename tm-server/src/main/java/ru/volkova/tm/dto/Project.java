package ru.volkova.tm.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.entity.IWBS;

import javax.persistence.Entity;

@Entity(name = "project")
@NoArgsConstructor
public class Project extends AbstractOwnerEntity implements IWBS {

    @NotNull
    public String toString() {
        return id + ": " + getName() + "; " +
                "created: " + getCreated() +
                "started: " + getDateStart();
    }

}
