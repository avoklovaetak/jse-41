package ru.volkova.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.dto.Project;
import ru.volkova.tm.dto.Session;
import ru.volkova.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @SneakyThrows
    @Nullable
    @WebMethod
    Project changeProjectOneStatusById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    );

    @SneakyThrows
    @Nullable
    @WebMethod
    Project changeProjectStatusByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    );

    @WebMethod
    public void clearProject(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @SneakyThrows
    @NotNull
    @WebMethod
    List<Project> findAllProjects(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @Nullable
    @WebMethod
    Project findProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    );

    @Nullable
    @WebMethod
    Project findProjectByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    );

    @Nullable
    @WebMethod
    Project findProjectByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @WebMethod
    void removeProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    );

    @WebMethod
    void removeProjectByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @Nullable
    @WebMethod
    Project updateProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );

    @WebMethod
    void addProjectByUser(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );

}
